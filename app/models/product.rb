class Product < ApplicationRecord
  resourcify
  mount_uploader :image, ImageUploader
  serialize :image, JSON
  belongs_to :user, optional: true

  validates :name, :brand, :category, :price, :image, presence: true
  validates :description, length: {maximum: 100}
  validates :name, length: {maximum: 50}
  validates :brand, length: {maximum: 20}
  validates :category, length: {maximum: 20}
  validates :price, numericality: {only_integer: true}, length: {maximum: 10}

  BRAND = %w{Rode Sony Warm Audio Thomann Yamaha}
  CATEGORY = %w{Guitar Piano Microphone}
end
