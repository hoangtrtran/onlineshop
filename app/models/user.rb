class User < ApplicationRecord
  rolify
  before_create :confirmation_token
  after_create :assign_default_role
  validates :username, presence: true, uniqueness: {case_sensitive: false},
            length: {minimum: 3, maximum: 25}

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, uniqueness: {case_sensitive: false},
            length: {maximum: 50},
            format: { with: VALID_EMAIL_REGEX }
  has_secure_password

  def assign_default_role
    self.add_role(:customer) if self.roles.blank?
  end

  def confirm_email_activate
    self.email_confirmed = true
    self.confirm_token = nil
    save!(:validate => false)
  end

  private
    def confirmation_token
      if self.confirm_token.blank?
        self.confirm_token = SecureRandom.urlsafe_base64.to_s
      end
    end
end
