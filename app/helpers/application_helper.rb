module ApplicationHelper
  def full_title(page_title)
    base_title = "Music Online Shop"
    if page_title.empty?
      base_title
    else
      "#{base_title} | #{page_title}"
    end
  end

  def image_tag(source, options={})
    super(source, options) if source.present?
  end

end
