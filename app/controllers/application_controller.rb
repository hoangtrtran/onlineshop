class ApplicationController < ActionController::Base

  helper_method :current_user, :logged_in? #make it availble to other class

  def current_user
    user_id = session[:user_id] #userId that store by the browser
    if user_id
      #if the current user already searched in the database then there is no need to hit database again
      @current_user ||= User.find_by_id(user_id)
    end
  end

  def logged_in?
    !!current_user
  end

end
