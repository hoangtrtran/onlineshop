class SessionsController < ApplicationController
  before_action :set_session, only: [:show, :edit, :update, :destroy]

  # GET /sessions
  # GET /sessions.json
  def index
    @sessions = Session.all
  end

  # GET /sessions/1
  # GET /sessions/1.json
  def show
  end

  # GET /sessions/new
  def new
    @session = Session.new
  end

  # GET /sessions/1/edit
  def edit
  end

  # POST /sessions
  # POST /sessions.json
  def create
    @session = Session.new(session_params)

    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.email_confirmed
        session[:user_id] = user.id
        flash[:success] = "Your have successfully logged in!"
        redirect_to root_path
      else
        flash[:danger] = 'Please activate your account by following the
        instructions in the account confirmation email you received to proceed'
        redirect_to login_path
      end
    else
      flash[:danger] = "Your email adresse or password was not corrected!"
      redirect_to login_path
    end

  end

  def destroy
    session[:user_id] = nil
    flash[:success] = "Your have successfully logged out!"
    redirect_to root_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_session
      @session = Session.find_by_id(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def session_params
      params.require(:session).permit(:email, :password)
    end
end
