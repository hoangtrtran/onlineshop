class UserMailer < ApplicationMailer

  def registration_confirmation(user)
    @user = user
    mail(to: @user.email, subject: 'Sign up Confirmation')
  end
end
