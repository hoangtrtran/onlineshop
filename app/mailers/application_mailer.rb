class ApplicationMailer < ActionMailer::Base
  default from: "mrliamtran@gmail.com"
  layout 'mailer'
end
