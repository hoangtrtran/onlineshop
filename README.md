# Musik Online Shop
---

# Clone the repository

> git clone git@bitbucket.org:hoangtrtran/onlineshop.git

> cd onlineshop

---

# Check your Ruby version

> ruby -v

If its not Ruby Version: 2.6.5p114 you need to install the right version

> rbenv install 2.6.5

---

# Dependencies

> Using bundle and yarn

---

# Initialize the database

> rails db:create db:migrate db:seed

---

# Run Server

> rails s

---

# Test Project

> rails test

---

# Deploy with Heroku

> git push heroku develop:master

---

# Run migration after deploy with Heroku

> heroku run rails db:migrate

---
