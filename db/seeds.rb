# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
admin_user = User.new(username: 'admin', email: 'admin@gmail.com', password: 'admin', password_confirmation: 'admin')
admin_user.add_role :admin
admin_user.confirm_email_activate
