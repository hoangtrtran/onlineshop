require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @base_title = "Music Online Shop"
  end

  test "should get root" do
    get root_url
    assert_response :success
    assert_select "title", "#{@base_title}"
  end

  test "should get about" do
    get pages_about_url
    assert_response :success
    @page_title = "About"
    assert_select "title", "#{@base_title} | #{@page_title}"
  end
end
