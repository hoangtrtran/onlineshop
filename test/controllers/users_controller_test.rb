require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
    @username = 'hiliam'
    @email = 'hiliam@havefun.de'
    @email_wrong = 'hiliam@havefun'
    @password = 'password'
  end

  test "should get index" do
    get users_url
    assert_response :success
  end

  test "should get new" do
    get new_user_url
    assert_response :success
  end

  #test "should create user" do
  #  assert_difference('User.count') do
  #    post users_url, params: { user: { email: @email, username: @username, password: @password, password_confirmation: @password } }
  #  end

  #  assert_redirected_to user_url(User.last)
  #end

  test "should not create user" do
    post users_url, params: { user: { email: @email_wrong, username: @username, password: @password, password_confirmation: @password } }

    assert_template :new
  end

  test "should show user" do
    get user_url(@user)
    assert_response :success
  end

  test "should get edit" do
    get edit_user_url(@user)
    assert_response :success
  end

  test "should update user" do
    patch user_url(@user), params: { user: { email: @email, username: @username, password: @password, password_confirmation: @password } }
    assert_redirected_to user_url(@user)
  end

  test "should not update user" do
    patch user_url(@user), params: { user: { email: @email_wrong, username: @username, password: @password, password_confirmation: @password } }
    assert_template :edit
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete user_url(@user)
    end

    assert_redirected_to users_url
  end

  test "authorization test user cannot create new product" do
    ability = Ability.new(@user)
    assert ability.can?(:read, Product.new)
    assert ability.cannot?(:create, Product.new)
  end

  test "authorization test admin can create new product" do
    @user.add_role :admin
    ability = Ability.new(@user)
    assert ability.can?(:create, Product.new)
  end
end
