require 'test_helper'
include ActionDispatch::TestProcess::FixtureFile

class ProductTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @product = Product.new
    @product.name = 'Yamaha YDP-144 B Arius Set'
    @product.brand = 'Yamaha'
    @product.category = 'Piano'
    @product.price = 10
    @product.image = fixture_file_upload('test/fixtures/files/YDP-144B.jpg', 'image/jpg')
  end

  test 'product should be valid' do
    assert @product.valid?
  end

  test 'name should be present' do
    @product.name = ''
    assert_not @product.valid?
  end

  test 'brand should be present' do
    @product.brand = ''
    assert_not @product.valid?
  end

  test 'category should be present' do
    @product.category = ''
    assert_not @product.valid?
  end

  test 'price should be present' do
    @product.price = ''
    assert_not @product.valid?
  end

  test 'image should be present' do
    @product.image = ''
    assert_not @product.valid?
  end

  test 'name should not be too long' do
    @product.name = 'Yamaha' * 100
    assert_not @product.valid?
  end

  test 'brand should not be too long' do
    @product.brand = 'Yamaha' * 100
    assert_not @product.valid?
  end

  test 'category should not be too long' do
    @product.category = 'Piano' * 100
    assert_not @product.valid?
  end

  test 'price should not be too long' do
    @product.price = 100000000000
    assert_not @product.valid?
  end

end
