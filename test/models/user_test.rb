require 'test_helper'

class UserTest < ActiveSupport::TestCase
  require 'test_helper'
  include ActionDispatch::TestProcess::FixtureFile

  class UserTest < ActiveSupport::TestCase
    # test "the truth" do
    #   assert true
    # end

    def setup
      @user = User.new
      @user.username = 'helloliam'
      @user.email = 'helloliam@havefun.de'
      @user.password = 'password'
    end

    test 'user should be valid' do
      assert @user.valid?
    end

    test 'username should be present' do
      @user.username = ''
      assert_not @user.valid?
    end

    test 'user should not be too short' do
      @user.username = 'hi'
      assert_not @user.valid?
    end

    test 'user should not be too long' do
      @user.username = 'hello' * 100
      assert_not @user.valid?
    end

    test "username should be unique" do
      @user.save
      @duplicate_user = User.new
      @duplicate_user.username = 'helloliam'
      assert_not @duplicate_user.valid?
    end

    test "username case sensitive should be unique" do
      @user.save
      @duplicate_user = User.new
      @duplicate_user.username = 'helloLIAM'
      assert_not @duplicate_user.valid?
    end

    test 'email should be present' do
      @user.email = ''
      assert_not @user.valid?
    end

    test 'email wrong format 1' do
      @user.email = 'hi'
      assert_not @user.valid?
    end

    test 'email wrong format 2' do
      @user.email = 'hi.de'
      assert_not @user.valid?
    end

    test 'email wrong format 3' do
      @user.email = 'hi@c'
      assert_not @user.valid?
    end

    test 'email should not be too long' do
      @user.username = 'hello' * 100 + '@hello.com'
      assert_not @user.valid?
    end

    test "email should be unique" do
      @user.save
      @duplicate_user = User.new
      @duplicate_user.email = 'helloliam@havefun.de'
      assert_not @duplicate_user.valid?
    end

    test "email case sensitive should be unique" do
      @user.save
      @duplicate_user = User.new
      @duplicate_user.email = 'helloLIAM@havefun.de'
      assert_not @duplicate_user.valid?
    end

    test "password authenticate should be valid" do
      @user.password_confirmation = 'password'
      assert @user.valid?
    end

    test "password authenticate should not be valid" do
      @user.password_confirmation = 'wrongpassword'
      assert_not @user.valid?
    end
  end
end
