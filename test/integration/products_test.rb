require 'test_helper'

class ProductsTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  #
  test "cannot create a product" do
    post "/products",
         params: { product: { name: "" } }

    assert_template :new
  end


end
