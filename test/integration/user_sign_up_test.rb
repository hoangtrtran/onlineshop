require 'test_helper'

class UserSignUpTest < ActionDispatch::IntegrationTest
  test "valid signup information" do
    user = User.new
    user.username = "example"
    user.email = "user@example.com"
    user.password = "password"
    user.password_confirmation = "password"


    assert_difference "User.count", 1 do
      post users_path, params: {
          user: {
              username: "example",
              email: "user@example.com",
              password: "password",
              password_confirmation: "password"
          }
      }
    end
    follow_redirect!
    assert_template "user_mailer/registration_confirmation"
    assert_template "layouts/mailer"
    assert_template "pages/home"
    assert_template "layouts/_navigation"
    assert_template "layouts/application"
    assert_not flash.empty?

  end

  test "invalid signup information" do
    get signup_url
    assert_no_difference "User.count" do
      post users_path, params: {
          user: {
              username: " ",
              email: "user@example.com",
              password: "what_the_hell",
              password_confirmation: "password"
          }
      }
    end
    assert_template "users/new"
    assert_not flash.empty?
  end

  test "confirm email" do

  end

end
