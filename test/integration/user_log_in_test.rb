require 'test_helper'

class UserLogInTest < ActionDispatch::IntegrationTest
  def setup
    @user =  User.new(username: "example",
                      email: "user@example.com",
                      password: "password",
                      password_confirmation: "password",
                      confirm_token: "token")
  end

  test "login with valid information and confirm email followed by logout" do
    @user.confirm_email_activate
    get confirm_email_user_url(@user.confirm_token)
    get login_url
    post login_path, params: {
        session: { email: @user.email, password: "password" }
    }
    assert_not flash.empty?
    assert_redirected_to root_url
    follow_redirect!
    assert_select "a[href=?]", login_path,        count: 0
    assert_select "a[href=?]", logout_path

    delete logout_path
    assert_redirected_to root_url
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path,       count: 0
    assert_select "a[href=?]", user_path(@user),  count: 0
  end

  test "login with valid information without email confirmed" do
    get confirm_email_user_url("wrongtoken")
    get login_url
    @user.email_confirmed = false
    post login_path, params: {
        session: { email: @user.email, password: "password" }
    }
    assert_not flash.empty?
    assert_redirected_to login_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
  end

  test "login with invalid information" do
    get login_url
    post login_path, params: {
        session: { email: @user.email, password: "password"}
    }
    assert_not flash.empty?
    assert_redirected_to login_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", logout_path, count: 0
  end
end
